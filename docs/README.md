
## 博客docsify

### 仓库

[gitlab]( https://gitlab.com/xuyq123/myblog-docsify ) &ensp; [gitee]( https://gitee.com/xy180/myblog-docsify ) &ensp; [github]( https://github.com/scott180/myblog-docsify ) &ensp; [gitlab_myblog-docsify]( https://xuyq123.gitlab.io/myblog-docsify/ )  &ensp; [github_myblog-docsify]( https://scott180.github.io/myblog-docsify/ ) 
	
### 文档

```
- 博客docsify
  - [note](笔记/note.md)
  - [java](笔记/java.md)
  - [gitNote](笔记/gitNote.md)
  - [linuxNote](笔记/linuxNote-x.md)

- 数据库  
  - [mysqlNote](数据库/mysqlNote.md)
  - [oracleNote](数据库/oracleNote.md)
  - [mongo](数据库/mongo.md)
  - [数据库隔离级别](数据库/数据库隔离级别.md)
  - [mysql开启log-bin日志](数据库/mysql开启log-bin日志.md)

- 资料
  - [eclipse](资料/eclipse.md)
  - [docker](资料/docker.md)
  - [markdown常用语法](资料/markdown常用语法.md)
  - [git平台docsify布署markdown文件](资料/git平台docsify布署markdown文件.md)
  - [gitlab、github、gitee布署mkdocs主题仓库](资料/gitlab、github、gitee布署mkdocs主题仓库.md)
	
- 文档
  - [古文诗词](文档/古文诗词.md)
  - [多宝塔碑](文档/多宝塔碑.md)
  - [《心经》书法](文档/《心经》书法.md)
  - [书法练习轨迹ReadMe](文档/书法练习轨迹ReadMe.md)
  - [无为徐生](文档/无为徐生.md)


```

---
	
> `颜真卿-多宝塔碑` <br/>
![颜真卿-多宝塔碑]( https://xyqin.coding.net/p/my/d/imgs/git/raw/master/other/颜真卿-多宝塔碑.jpg )

